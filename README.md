# terraform

Install and manage Terraform

## Dependencies

* [polka.asdf](https://gitlab.com/discr33t/polka/asdf.git)
  _The asdf configs must be included in the users `playbook.yml` since no
  default configs are passed to the dependent role_

* [polka.bash](https://gitlab.com/discr33t/polka/asdf.git)
  _The bash configs must be included in the users `playbook.yml` since no
  default configs are passed to the dependent role_

## Role Variables

* `versions`:
    * Type: List
    * Usages: List of Terraform versions to install

* `global_version`:
    * Type: String
    * Usages: Terraform version to make global default

```
terraform:
  versions:
    - 0.11.8
    - 0.11.10
  global_version: 0.11.10
```

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - polka.terraform

## License

MIT
